Some models have multiple diffuse textures available and can share spec and normal maps.

barrier_concrete:
diff: barrierconcrete_diffuse
spec: barrierconcrete_spec
norl: barrierconcrete_normal 
	

barrier_metal:
diff: barriermetal_diffuse
spec: barriermetal_spec
norm: barriermetal_normal

barrier_smallwooden
diff: barrierconstruction_diffuse
spec: barrierconstruction_spec
norm: barrierconstruction_normal

barrier_wooden
diff: barrierwooden_diffuse
spec: barrierwooden_spec
norm: barrierwooden_normal

lightpole
diff: streetlight_diffuse
spec: streetlight_spec
norm: streetlight_normal

pedcrossing
diff: crossingdontwalk_diffuse   crossingwalk_diffuse
spec: crossing_spec
norm: crossing_normal

roadbarrel
diff: roadbarrel_diffuse
spec: roadbarrel_spec
norm: roadbarrel_normal

roadcone
diff: roadcone_diffuse
spec: roadcone_spec
norm: roadcone_normal

signinfodiamond
diff: signdeadend_diffuse    signpedcrossing_diffuse
spec: signdiamond_spec
norm: signdiamond_normal

signinfolargesquare
diff: signdonotenter_diffuse signnoleftturn_diffuse  signnorightturn_diffuse signnouturn_diffuse
spec: sign_specular
norm: sign_normal

signingosmall
diff: signnoparking_diffuse  signfirelane_diffuse
spec: sign_specular
norm: sign_normal

signstop
diff: signstop_diffuse
spec: signstop_spec
norm: signstop_normal

signyield
diff: signyield_diffuse
spec: signyield_spec
norm: signyield_normal

trafficlight
diff: trafficlightred_diffuse  trafficlightyellow_diffuse  trafficlightgreen_diffuse
spec: trafficlight_spec
norm: trafficlight_normal

trafficpolemultiple
diff: trafficpole_diffuse
spec: trafficpole_spec
norm: trafficpole_normal

trafficpolesingle
diff: trafficpole_diffuse
spec: trafficpole_spec
norm: trafficpole_normal

trashcan
diff: trashcan_diffuse
spec: trashcan_spec
norm: trashcan_normal
