﻿using UnityEngine;
using UnityEditor;

public class PlayerTypeAsset
{
	[MenuItem("Assets/Create/Player Type")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<PlayerType> ();
	}
}