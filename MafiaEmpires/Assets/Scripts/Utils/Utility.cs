﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility {

	public static float Angle(Vector3 lhs, Vector3 rhs){
		return Mathf.Asin((lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z)) * Mathf.Rad2Deg;
	}

	public static float Angle2(Vector3 lhs, Vector3 rhs){
		return Mathf.Acos((lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z)) * Mathf.Rad2Deg;
	}

	public static float Angle3(Vector3 lhs, Vector3 rhs){
		return Mathf.Atan((lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z)) * Mathf.Rad2Deg;
	}

	public static float HorizontalSlope(Vector3 lhs, Vector3 rhs){
		lhs.Normalize ();
		rhs.Normalize ();

		float slope = Angle2 (lhs, rhs) / 90;

		if(slope > 1){
			slope = -(1 - (slope - 1));
		}

		return slope;
	}

	public static Vector3 Rotate(Vector3 point, Vector3 pivot, Quaternion angle){
		return (angle * (point - pivot)) + pivot;
	}
}