﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Const {

	private static float halfSquareRoot2 = 0;

	public static float HalfSquareRoot2{
		get{
			if(halfSquareRoot2 > 0){
				return halfSquareRoot2;
			}
			return halfSquareRoot2 = Mathf.Sqrt(2) / 2;
		}
	}

	private static float squareRoot2 = 0;

	public static float SquareRoot2{
		get{
			if(squareRoot2 > 0){
				return squareRoot2;
			}
			return squareRoot2 = Mathf.Sqrt(2);
		}
	}
}