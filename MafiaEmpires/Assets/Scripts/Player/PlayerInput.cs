﻿using System;

[Serializable]
public struct PlayerInput{
	
	private float deltaYaw;
	private float deltaPitch;
	private bool forward;
	private bool backward;
	private bool strafeRight;
	private bool strafeLeft;
	private bool jump;
	private int forwardMovement;
	private int strafeMovement;

	public float DeltaYaw{
		get{
			return deltaYaw;
		}

		set{
			deltaYaw = value;
		}
	}

	public float DeltaPitch{
		get{
			return deltaPitch;
		}

		set{
			deltaPitch = value;
		}
	}

	public bool Forward{
		get{
			return forward;
		}

		set{
			forwardMovement = (forward = value).GetHashCode() - backward.GetHashCode();
		}
	}

	public bool Backward{
		get{
			return backward;
		}

		set{
			forwardMovement = forward.GetHashCode() - (backward = value).GetHashCode();
		}
	}

	public bool StrafeRight{
		get{
			return strafeRight;
		}

		set{
			strafeMovement = (strafeRight = value).GetHashCode() - strafeLeft.GetHashCode();
		}
	}

	public bool StrafeLeft{
		get{
			return strafeLeft;
		}

		set{
			strafeMovement = strafeRight.GetHashCode() - (strafeLeft = value).GetHashCode();
		}
	}

	public bool Jump{
		get{
			return jump;
		}

		set{
			jump = value;
		}
	}

	public int ForwardMovement{
		get{
			return forwardMovement;
		}
	}

	public int StrafeMovement{
		get{
			return strafeMovement;
		}
	}
}