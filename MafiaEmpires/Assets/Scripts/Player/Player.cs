﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))] // For triggerable colliders to register when player is attempting to trigger them.
[RequireComponent(typeof(CapsuleCollider))] // For raycasts to hit the player.
[RequireComponent(typeof(CharacterController))] // For player movement partial collision/velocity management.
[RequireComponent(typeof(NetworkIdentity))]
public class Player : NetworkBehaviour {

	[Header("Player (Note: Character Controller \"Min Move Distance\" must equal 0!)")]
	public CharacterController characterController = null;
	public CapsuleCollider capsuleCollider = null;
	public Rigidbody rigidBody = null;

	[Header("Network")]
	public float disconnectMessageTime = 10;
	[Range(1, 1024)]
	public int disconnectMessageLength = 1024;

	private static float deltaTime = 0;
	private bool disconnect = false;
	private float disconnectTime = 0;
	private bool updated = true;
	private int updates = 0;
	private int updatedUpdates = 0;
	private float latency = 0;
	private float positionVariance = 0;
	private float velocityVariance = 0;
	private float interpolationTime = 0;
	private Vector3 interpolationPosition = Vector3.zero;
	private float interpolationYaw = 0;

	[Header("Player")]
	[Range(0, 1024)]
	public int maximumNameLength = 32;
	public PlayerInput playerInput;
	public PlayerType playerType = null;

	private float health = 0;

	[Header("Movement")]
	[Range(0, 1)]
	public float maximumGroundSlope = 0.5f;
	[Range(0, 1)]
	public float maximumCeilingSlope = 0.5f;

	private bool collisionUpdate = false;

	private Vector3 velocity = Vector3.zero;

	private bool groundedUpdate = false;

	private int groundedUpdates = 0;
	private float groundedTime = 0;
	private float groundSlope = 0;
	private Vector3 groundNormal = Vector3.up;

	public string Name{
		get{
			return name;
		}
	}

	public float Latency{
		get{
			return latency;
		}
	}

	public float PositionVariance{
		get{
			return positionVariance;
		}
	}

	public float VelocityVariance{
		get{
			return velocityVariance;
		}
	}

	public Vector3 Position{
		get{
			return transform.position;
		}

		set{
			transform.position = value;
		}
	}

	public float Yaw{
		get{
			return transform.rotation.eulerAngles.y;
		}

		set{
			transform.rotation = Quaternion.Euler(0, value, 0);
		}
	}

	public float Height{
		get{
			if(characterController == null){
				if (capsuleCollider == null) {
					return 0;
				} else {
					return capsuleCollider.height;
				}
			}
			return characterController.height;
		}

		set{
			if(!(characterController == null)){
				characterController.height = value;
			}

			if(!(capsuleCollider == null)){
				capsuleCollider.height = value;
			}
		}
	}

	public Vector3 Velocity{
		get{
			return velocity;
		}

		set{
			velocity = value;
		}
	}

	public Vector3 RelativeVelocity{
		get{
			return transform.InverseTransformDirection(velocity);
		}

		set{
			velocity = transform.TransformDirection(value);
		}
	}

	public int GroundedUpdates{
		get{
			return groundedUpdates;
		}
	}

	public float Health{
		get{
			return health;
		}

		set{
			health = value;
		}
	}

	private void OnValidate(){
		if (characterController == null){
			characterController = GetComponent<CharacterController>();

			if (characterController == null){
				characterController = gameObject.AddComponent<CharacterController>();
			}
		}
		characterController.slopeLimit = 180;
		characterController.stepOffset = 0;

		if(capsuleCollider == null){
			capsuleCollider = GetComponent<CapsuleCollider> ();

			if(capsuleCollider == null){
				capsuleCollider = gameObject.AddComponent<CapsuleCollider> ();
			}
		}
		capsuleCollider.isTrigger = true;
		capsuleCollider.center = characterController.center;
		capsuleCollider.direction = 1;
		capsuleCollider.height = Height;
		capsuleCollider.radius = characterController.radius;

		if (rigidBody == null){
			rigidBody = GetComponent<Rigidbody>();

			if (rigidBody == null){
				rigidBody = gameObject.AddComponent<Rigidbody>();
			}
		}
		rigidBody.mass = 1;
		rigidBody.drag = 0;
		rigidBody.angularDrag = 0;
		rigidBody.isKinematic = false;
		rigidBody.detectCollisions = true;
		rigidBody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
		rigidBody.useGravity = false;
		rigidBody.constraints = RigidbodyConstraints.FreezeAll;
	}

	public void Disconnect(float time, string message){
		if (isServer) {
			disconnect = true;
			if (disconnectMessageTime > 1) {
				if (time > 1) {
					disconnectTime = Mathf.Clamp (time, disconnectMessageTime, float.MaxValue);
				} else {
					disconnectTime = disconnectMessageTime;
				}
			} else if (disconnectTime > 0) {
				disconnectTime = disconnectMessageTime;
			}
			if(!(message == null) && (disconnectMessageLength > 0)){
				RpcDisconnectMessage (
					message.Substring(
						0, 
						Mathf.Clamp(
							message.Length, 
							0, 
							disconnectMessageLength
						)
					)
				);
			}
		}
	}

	public void CancelDisconnect(){
		if(isServer){
			disconnect = false;
			disconnectTime = 0;
		}
	}

	[ClientRpc]
	private void RpcDisconnectMessage(string message){
		if(!(message == null)){
			
		}
	}

	private void Disconnect(){
		if (isServer && disconnect) {
			if (disconnectTime > 0) {
				disconnectTime -= Time.deltaTime;
			}

			if(disconnectTime <= 0){
				if (!(connectionToServer == null)) {
					connectionToServer.Disconnect ();
				}
				if (!(connectionToClient == null)) {
					connectionToClient.Disconnect ();
				}
			}
		}
	}

	private void InterpolateTransformation(){
		if (interpolationTime < Time.fixedDeltaTime) {
			interpolationTime += Time.deltaTime;
		}
		Position = Vector3.Lerp (Position, interpolationPosition, interpolationTime / Time.fixedDeltaTime);
		Yaw = Mathf.LerpAngle (Yaw, interpolationYaw, interpolationTime / Time.fixedDeltaTime);
	}

	private static void UpdateDeltaTime(){
		deltaTime = Time.deltaTime;
	}

	private void OnControllerColliderHit(ControllerColliderHit controllerColliderHit){
		if (isServer || isLocalPlayer) {
			collisionUpdate = true;

			UpdateDeltaTime ();

			float collisionSlope = Utility.HorizontalSlope (Vector3.up, controllerColliderHit.normal);
			if ((controllerColliderHit.normal == Vector3.up) || (collisionSlope > 0)) {
				if (collisionSlope <= maximumGroundSlope) {
					groundSlope = collisionSlope;
					groundNormal = controllerColliderHit.normal;

					groundedUpdate = true;
					groundedUpdates = Mathf.Clamp (groundedUpdates + 1, 1, int.MaxValue);
					groundedTime = Mathf.Clamp (groundedTime + deltaTime, 0, float.MaxValue);
				}
			}
		}
	}

	private void UpdateControllerColliderHit(){
		if (isServer || isLocalPlayer) {
			if (collisionUpdate) {
				collisionUpdate = false;
			}

			UpdateDeltaTime ();

			if (groundedUpdate) {
				groundedUpdate = false;
			} else {
				groundSlope = 0;
				groundNormal = Vector3.up;
				groundedUpdates = Mathf.Clamp(groundedUpdates - 1, int.MinValue, 0);
				groundedTime = Mathf.Clamp (groundedTime - deltaTime, float.MinValue, 0);
			}
		}
	}

	[ClientRpc]
	private void RpcRequestPlayerUpdate(float latency, int updates, int updatedUpdates){
		if (isServer || isLocalPlayer) {
			if (isLocalPlayer) {
				this.latency = latency;
				this.updates = updates;
				this.updatedUpdates = updatedUpdates;
			}
			CmdSynchronizePlayer (Position, Velocity, Yaw, playerInput.DeltaYaw);
		}
	}

	[Command]
	private void CmdSynchronizePlayer(Vector3 position, Vector3 velocity, float yaw, float deltaYaw){
		updated = true;

		interpolationTime = 0;

		playerInput.DeltaYaw = deltaYaw;

		positionVariance = Vector3.Distance (interpolationPosition + (velocity * Latency), position);
		velocityVariance = Vector3.Distance (Velocity, velocity);
		if ((positionVariance > playerType.maximumPositionVariance) || (velocityVariance > playerType.maximumVelocityVariance)) {
			interpolationPosition = Position;
			interpolationYaw = Yaw;

			RpcForceLocalPlayer (
				interpolationPosition, 
				Velocity, 
				interpolationYaw, 
				Latency, 
				PositionVariance, 
				VelocityVariance
			);
		} else {
			Position = position;
			Velocity = velocity;
			Yaw = yaw;

			interpolationPosition = Position;
			interpolationYaw = Yaw;

			RpcSynchronizePlayer (
				interpolationPosition, 
				Velocity, 
				interpolationYaw, 
				Health, 
				Latency, 
				PositionVariance, 
				VelocityVariance
			);
		}
	}

	[ClientRpc]
	public void RpcForceLocalPlayer(Vector3 position, Vector3 velocity, float yaw, float latency, float positionVariance, float velocityVariance){
		if (!isServer) {
			if (isLocalPlayer) {
				Position = position;
				Velocity = velocity;
				Yaw = yaw;
				this.latency = latency;
				this.positionVariance = positionVariance;
				this.velocityVariance = velocityVariance;
			} else {
				Position = interpolationPosition;
				Velocity = velocity;
				Yaw = interpolationYaw;
				this.latency = latency;
				this.positionVariance = positionVariance;
				this.velocityVariance = velocityVariance;

				interpolationTime = 0;
				interpolationPosition = position;
				interpolationYaw = yaw;
			}
		}
	}

	[ClientRpc]
	private void RpcSynchronizePlayer(Vector3 position, Vector3 velocity, float yaw, float health, float latency, float positionVariance, float velocityVariance){
		if (!isServer) {
			if (isLocalPlayer) {
				Health = health;
				this.latency = latency;
				this.positionVariance = positionVariance;
				this.velocityVariance = velocityVariance;

				interpolationPosition = position;
				interpolationYaw = yaw;
			} else {
				Velocity = velocity;
				Health = health;
				this.latency = latency;
				this.positionVariance = positionVariance;
				this.velocityVariance = velocityVariance;

				Position = interpolationPosition;
				Yaw = interpolationYaw;

				interpolationTime = 0;
				interpolationPosition = position;
				interpolationYaw = yaw;
			}
		}
	}

	private void Update(){
		if (!isServer && !isLocalPlayer){
			InterpolateTransformation();
		}
	}

	void FixedUpdate(){
		if (isLocalPlayer){
			UpdateControllerColliderHit();

			MovePlayer();

			if (isServer) {
				latency = deltaTime / 2;

				if ((updates == int.MaxValue) || (updatedUpdates == int.MaxValue)) {
					updates = 0;
					updatedUpdates = 0;
				}
				updates++;
				updatedUpdates++;

				RpcSynchronizePlayer (
					Position, 
					Velocity, 
					Yaw, 
					Health, 
					Latency, 
					PositionVariance, 
					VelocityVariance
				);
			}
		} else if (isServer){
			Disconnect ();

			UpdateControllerColliderHit();

			MovePlayer();

			if ((updates == int.MaxValue) || (updatedUpdates == int.MaxValue)) {
				updates = 0;
				updatedUpdates = 0;
			}
			updates++;
			if (updated) {
				updated = false;
				updatedUpdates++;
//				RpcRequestPlayerUpdate (latency + (deltaTime / 2), updates, updatedUpdates);
				latency = 0;
			}

			if ((float.MaxValue - deltaTime) > latency) {
				latency += deltaTime;
			} else {
				latency = float.MaxValue;
			}
		}
	}

	private void MovePlayer(){
		UpdateDeltaTime();
		ShiftDirection();
		CalculateMovementVelocity();
		CalculateVelocityWithEnvironmentalForces();
		Move();
	}

	private void ShiftDirection(){
		if (groundedUpdates > 1) {
			Vector3 relativeGroundNormal = transform.InverseTransformDirection (groundNormal);
			Vector3 relativeVelocity = Utility.Rotate(
				RelativeVelocity, 
				Vector3.zero, 
				Quaternion.FromToRotation(
					relativeGroundNormal, 
					Vector3.up
				)
			);

			if ((playerInput.StrafeRight && relativeVelocity.x < 0) || (playerInput.StrafeLeft && relativeVelocity.x > 0)) {
				relativeVelocity.x = 0;
			}
			if ((playerInput.Forward && relativeVelocity.z < 0) || (playerInput.Backward && relativeVelocity.z > 0)) {
				relativeVelocity.z = 0;
			}

			RelativeVelocity = Utility.Rotate(
				relativeVelocity, 
				Vector3.zero, 
				Quaternion.FromToRotation(
					Vector3.up, 
					relativeGroundNormal
				)
			);
		}
	}

	private void CalculateMovementVelocity(){
		if (groundedUpdates > 0) {
			if (playerInput.Jump) {
				if (velocity.y < 0) {
					velocity.y = 0;
				}
				velocity.y += playerType.jumpSpeed;
			}
				
			float accelerationDirectional = 0;

			if (Mathf.Abs (playerInput.ForwardMovement) == Mathf.Abs (playerInput.StrafeMovement)) {
				accelerationDirectional = playerType.acceleration * deltaTime * Const.HalfSquareRoot2;
			} else {
				accelerationDirectional = playerType.acceleration * deltaTime;
			}

			Vector3 relativeVelocity = RelativeVelocity + Utility.Rotate (
				Struct.Vector (
					playerInput.StrafeMovement * accelerationDirectional, 
					0, 
					playerInput.ForwardMovement * accelerationDirectional
				), 
				Vector3.zero, 
				Quaternion.FromToRotation (
					Vector3.up, 
					transform.InverseTransformDirection(groundNormal)
				)
			);
			float relativeVelocityY = relativeVelocity.y;
			relativeVelocity.y = 0;

			if (relativeVelocity.magnitude > playerType.speed) {
				relativeVelocity = playerType.speed * relativeVelocity.normalized;
			}

			relativeVelocity.y = relativeVelocityY;
			RelativeVelocity = relativeVelocity;
		}
	}

	private void CalculateVelocityWithEnvironmentalForces(){
		velocity += Physics.gravity * deltaTime;

		if ((groundedUpdates > 1) &&
			!playerInput.Forward &&
			!playerInput.Backward &&
			!playerInput.StrafeRight &&
			!playerInput.StrafeLeft) {
			Vector3 groundVelocity = Utility.Rotate(
				velocity, 
				Vector3.zero, 
				Quaternion.FromToRotation(
					groundNormal, 
					Vector3.up
				)
			);

			groundVelocity = Struct.Vector(
				0, 
				groundVelocity.y, 
				0
			) + Utility.Target (
				Struct.Vector(
					groundVelocity.x, 
					0, 
					groundVelocity.z
				), 
				0, 
				playerType.acceleration * deltaTime
			);

			velocity = Utility.Rotate(
				groundVelocity, 
				Vector3.zero, 
				Quaternion.FromToRotation(
					Vector3.up, 
					groundNormal
				)
			);
		}
	}

	private void Move(){
		Yaw += playerInput.DeltaYaw;

		if (!(characterController == null)) {
			characterController.Move (velocity * deltaTime);
			velocity = characterController.velocity;
		}
	}
}