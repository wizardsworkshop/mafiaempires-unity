﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerType : ScriptableObject {
	
	[Header("Network")]
	public float maximumPositionVariance = 1;
	public float maximumVelocityVariance = 25;

	[Space(1)]

	[Header("Stats")]
	public float health = 100;

	[Space(1)]

	[Header("Movement (Recommended Gravity: -25)")]
	public float acceleration = 100;
	public float speed = 100;
	public float jumpSpeed = 10;
}